require 'json'

#numero de pokemons distintos
def unique_pkm pk_list
    unique_pkm = []
    pk_list.each do |pkm|
        #pega o nome do pokemon, ou seja, o primeiro elemento antes do '-'
        pkm_name = pkm['name'].split('-')[0]
        if unique_pkm.include?(pkm_name)
            next
        end
        unique_pkm << pkm_name
    end
    unique_pkm
end

def top_ocurrence pk_list, topN
    name_ocurrence = {}
    pk_list.each do |pkm|
        pkm_name = pkm['name'].split('-')[0]
        if name_ocurrence.key?(pkm_name)
           name_ocurrence[pkm_name] += 1
        else
            #cria chave com o nome do pokemon e adiciona um
            name_ocurrence[pkm_name] = 1
        end
    end
    #ordena o hash pelo valor de casa chave
    name_ocurrence = name_ocurrence.sort { |a, b| b[1] <=> a[1] }

    name_ocurrence[0..(topN-1)]
end

def top_letras pk_list 
    pokemons = unique_pkm pk_list
    pokemons_single_letter = []
    pokemons.each do |name|
        pokemons_single_letter << name[0]
    end
    
    letter_ocurrence = {}
    pokemons_single_letter.each do |letter|
        if letter_ocurrence.key?(letter)
            letter_ocurrence[letter] +=1
        else
            letter_ocurrence[letter] = 1
        end
    end

    letter_ocurrence = letter_ocurrence.sort { |a, b| b[1] <=> a[1] }

    letter_ocurrence[0..4]
end

file = File.read('pokemons.json')

#pega somente o array contendo os pokemons
pokemons = JSON.parse(file)['results']


puts "Temos #{unique_pkm(pokemons).length} pokémons únicos."
puts ""

topP = top_ocurrence pokemons, 10

puts "Os pokémons mais populares são:"
topP.each do |name, count|
    puts "#{name}: #{count} encontrados"
end 

puts ""

topL = top_letras pokemons

puts "As iniciais mais comuns são:"
topL.each do |letter, count|
    puts "#{letter}: #{count} encontrados"
end 

